# MyVSCode

## nodeJS插件

```
Auto Close Tag
Auto Rename Tag
Beautify
Chinese
CSS peek
Document This
ESlint
filesize
Gitlens-Git supercharged
HTML CSS support
HTML snippets
Java Server pages
Language Support for Java
Live server
open in browser
Path Autocomplete
Path Intellisense
PostCSS syntax
postcss-sugarss-language
Vetur
VS Code CSS Comments
vscode-icons
Vue 2 Snippets
VueHelper
```

# Go配置需要的源码

将src.rar解压到%GOPATH%路径，然后在该路径下执行下面命令：

```sh
go install github.com/mdempsky/gocode
go install github.com/uudashr/gopkgs/cmd/gopkgs
go install github.com/ramya-rao-a/go-outline
go install github.com/acroca/go-symbols
go install golang.org/x/tools/cmd/guru
go install golang.org/x/tools/cmd/gorename
go install github.com/go-delve/delve/cmd/dlv
go install github.com/stamblerre/gocode
go install github.com/rogpeppe/godef
go install github.com/sqs/goreturns
go install golang.org/x/lint/golint
```

